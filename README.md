# Machine learning curriculum

Learning machine learning on my own!

## [First plan, 3 steps](https://www.youtube.com/watch?v=7q_OJvQQ7vY)

1. Learn python

  1. [youtube course](https://www.youtube.com/watch?v=CcpyfWnZGNc)

  1. [book](https://automatetheboringstuff.com/)

  1. Learn everything else about Python on the fly.

1. Learn ML (high-level intro)
  
  1. [ML course](https://www.coursera.org/learn/machine-learning)

  1. [Deep Learning course](https://www.coursera.org/specializations/deep-learning)

  1. + at least 1 open-source project + 1 blog

1. Learn ML (middle-level intro)

  1. [fast ai course](https://course.fast.ai/)

  1. [second fast ai course](https://course19.fast.ai/part2)

  1. Again code, code, code. Open-source on GitHub.

  1. Start reading research papers and implement 1 paper from scratch.

1. Learn ML (pro)

  1. [MML book](https://mml-book.github.io/book/mml-book.pdf)

  1. Supplements:
    
    1. [3B1B LA](https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)

    1. [3B1B Calculus](https://www.youtube.com/watch?v=fNk_zzaMoSs&list=PLZHQObOWTQDPD3MizzM2xVFitgF8hE_ab)

    1. [chapter 5](https://jakevdp.github.io/PythonDataScienceHandbook/index.html)

1. Learn deep learning (pro)
  
  1. [book](https://github.com/janishar/mit-deep-learning-book-pdf) (you'll find a good pdf in this GitHub repo)

1. BONUS TIPS:
  
  1. [learn how to learn course](https://www.coursera.org/learn/learning-how-to-learn)

  1. [podcast](https://www.youtube.com/user/lexfridman)

  1. [Twitter the people I follow] (https://twitter.com/gordic_aleksa)

## [Second plan - 5 steps](https://www.youtube.com/watch?v=IMMDPzECrf0)

1. Step 1 - Learn Python, data science tools and machine learning concepts

  1. [Elements of AI](https://www.elementsofai.com/)

  1. [Python for Everybody on Coursera](https://www.coursera.org/specializations/python)

  1. [Learn Python by freeCodeCamp](https://www.youtube.com/watch?v=rfscVS0vtbw)

  1. [Corey Schafer's Anaconda Tutorial](https://www.youtube.com/watch?v=YJC6ldI3hWk)

  1. [Bonus: My Anaconda Blog Post](https://www.mrdbourke.com/get-your-computer-ready-for-machine-learning-using-anaconda-miniconda-and-conda/)

  1. [Corey Schafer's Jupyter Notebook Tutorial](https://www.youtube.com/watch?v=HW29067qVWk)

1. Step 2 - Learn data manipulation, analysis and visualization with pandas, NumPy and Matplotlib

  1. [Applied Data Science with Python](https://www.coursera.org/specializations/data-science-python?)

  1. [Codebasics Pandas series](https://www.youtube.com/playlist?list=PLeo1K3hjS3uuASpe-1LjfG5f14Bnozjwy)

  1. [freeCodeCamp NumPy video](https://www.youtube.com/watch?v=QUT1VHiLmmI)

  1. [Sentdex Matplotlib series](https://www.youtube.com/playlist?list=PLQVvvaa0QuDfefDfXb9Yf0la1fPDKluPF)

1. Step 3 - Learn machine learning with scikit-learn

  1. [Data School scikit-learn series](https://www.youtube.com/playlist?list=PL5-da3qGB5ICeMbQuqbbCOQWcS6OYBr5A)

  1. [Bonus: fastai machine learning course](https://course18.fast.ai/ml)


1. Step 4 - Learn deep learning and neural networks

  1. [Andrew Ng’s deep learning specialization on Coursera](https://www.coursera.org/specializations/deep-learning)

  1. [fastai deep learning curriculum](https://fast.ai)

1. Step 5 - Extra curriculum & books

  1. [My favourite machine learning books (video)](https://www.youtube.com/watch?v=7R08MPXxiFQ)

  1. [Python for Data Analysis by Wes McKinney](https://amzn.to/2Z1QZNp)

  1. [Hands-on Machine Learning with scikit-learn and TensorFlow by Aurelien Geron](https://amzn.to/2Q8iy4v)

  1. [The Mechanics of Machine Learning by Terrence Parr and Jeremy Howard](https://mlbook.explained.ai/)

  1. [A Gentle Introduction to Exploratory Data Analysis (project example)](https://towardsdatascience.com/a-gent)

  1. [How to work on your own machine learning projects (article)](https://towardsdatascience.com/how-to.)

